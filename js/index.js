document.querySelectorAll('img').forEach(item => item.setAttribute('alt', 'photo'));

// Service section


let text = document.querySelector('.service-text');
let webButton = document.querySelector('.service > li:first-child');
let graphicButton = document.querySelector('.service > li:nth-child(2)');
let supportButton = document.querySelector('.service > li:nth-child(3)');
let appButton = document.querySelector('.service > li:nth-child(4)');
let marketingButton = document.querySelector('.service > li:nth-child(5)');
let seoButton = document.querySelector('.service > li:last-child');
let img = document.querySelector('.service-info > img');
let option = {
    "once": true,
}

function web(){
    text.innerText = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. In, porro.';
    webButton.style.backgroundColor = '#18CFAB';
    img.removeAttribute('src');
    img.setAttribute('src', 'img/web-design/web-design2.jpg');
    document.querySelectorAll('.service > li:not(.service > li:first-child').forEach(item => item.style.backgroundColor = 'white');
}
function gr(){
    text.innerText = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati natus, est ut vitae fugit alias.';
    graphicButton.style.backgroundColor = '#18CFAB';
    img.removeAttribute('src');
    img.setAttribute('src', 'img/web-design/web-design3.jpg');
    document.querySelectorAll('.service > li:not(.service > li:nth-child(2))').forEach(item => item.style.backgroundColor = 'white');
}
function sp(){
    text.innerText = 'Lorem ipsum dolor sit.';
    supportButton.style.backgroundColor = '#18CFAB';
    img.removeAttribute('src');
    img.setAttribute('src', 'img/web-design/web-design4.jpg');
    document.querySelectorAll('.service > li:not(.service > li:nth-child(3))').forEach(item => item.style.backgroundColor = 'white');
}
function app(){
    text.innerText = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi magni id dolor repellat adipisci deleniti tempora nostrum quam iste fugit.';
    appButton.style.backgroundColor = '#18CFAB';
    img.removeAttribute('src');
    img.setAttribute('src', 'img/web-design/web-design5.jpg');
    document.querySelectorAll('.service > li:not(.service > li:nth-child(4))').forEach(item => item.style.backgroundColor = 'white');
}
function mark(){
    text.innerText = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.';
    marketingButton.style.backgroundColor = '#18CFAB';
    img.removeAttribute('src');
    img.setAttribute('src', 'img/web-design/web-design6.jpg');
    document.querySelectorAll('.service > li:not(.service > li:nth-child(5))').forEach(item => item.style.backgroundColor = 'white');
}
function seo(){
    text.innerText = 'Lorem, ipsum.';
    seoButton.style.backgroundColor = '#18CFAB';
    img.removeAttribute('src');
    img.setAttribute('src', 'img/web-design/web-design7.jpg');
    document.querySelectorAll('.service > li:not(.service > li:last-child)').forEach(item => item.style.backgroundColor = 'white');
}
webButton.addEventListener('click', web);
graphicButton.addEventListener('click', gr);
supportButton.addEventListener('click', sp);
appButton.addEventListener('click', app);
marketingButton.addEventListener('click', mark);
seoButton.addEventListener('click', seo);
web();

// Work section

let loadBtn = document.querySelector('.load-btn');
let hidden = document.querySelector('.load-grid');
hidden.style.display = 'none';
let hidden2 = document.querySelectorAll('.load-grid > img');
hidden2.forEach(item => item.style.display = 'block');

function loader(){
    loadBtn.remove();
    hidden.insertAdjacentHTML('beforebegin', '<div class=loading></div>');
    function del(){
        document.querySelector('.loading').remove();
        hidden2.forEach(item => item.style.display = 'inline');
        hidden.style.display = 'grid';
    }
    window.setTimeout(del, 2500);
}
loadBtn.addEventListener('click',loader, option);

let hover = document.querySelectorAll('.images > img');
let hov2 = document.querySelectorAll('.load-grid > img');
function hov(event){
    let target = event.target;
    target.insertAdjacentHTML('beforebegin', '<div class="category"><img src="img/shapes/ellipse.png"><img src="img/shapes/comb-shape.png"><img src="img/shapes/ellipse-2.png"><img src="img/shapes/white-square.png"><h4>CREATIVE DESIGN</h4><h5>Web Design</h5></div>');
    target.style.display = 'none';
}
function notHov(event){
    let target = event.target;
    document.querySelector('.category').remove();
    target.style.display = 'block';
}
hover.forEach(item => item.addEventListener('mouseover', hov));
hover.forEach(item => item.addEventListener('mouseout', notHov));
hov2.forEach(item => item.addEventListener('mouseover', hov));
hov2.forEach(item => item.addEventListener('mouseout', notHov));

let allCtg = document.querySelector('.works-list > li:first-child');
let graphicDesign = document.querySelector('.works-list > li:nth-child(2)');
let webDesign = document.querySelector('.works-list > li:nth-child(3)');
let landingPages = document.querySelector('.works-list > li:nth-child(4)');
let wordpress = document.querySelector('.works-list > li:last-child');


graphicDesign.addEventListener('click', function gdActive(){
    let gd = document.querySelectorAll('.images > img:first-child, .images > img:nth-child(5), .images > img:nth-child(9)');
    gd.forEach(item => item.classList.add('active'));
    if(!gd.forEach(item => item.classList.contains('active'))){
        document.querySelectorAll('.images > img:not(.images > img:first-child, .images > img:nth-child(5), .images > img:nth-child(9))').forEach(item => item.classList.remove('active'));
    }
});
webDesign.addEventListener('click', function wdActive(){
    let wd = document.querySelectorAll('.images > img:nth-child(2), .images > img:nth-child(6), .images > img:nth-child(10)');
    wd.forEach(item => item.classList.add('active'));
    if(!wd.forEach(item => item.classList.contains('active'))){
        document.querySelectorAll('.images > img:not(.images > img:nth-child(2), .images > img:nth-child(6), .images > img:nth-child(10))').forEach(item => item.classList.remove('active'));
    }
});
landingPages.addEventListener('click', function lpActive(){
    let lp = document.querySelectorAll('.images > img:nth-child(3), .images > img:nth-child(7), .images > img:nth-child(11)');
    lp.forEach(item => item.classList.add('active'));
    if(!lp.forEach(item => item.classList.contains('active'))){
        document.querySelectorAll('.images > img:not(.images > img:nth-child(3), .images > img:nth-child(7), .images > img:nth-child(11))').forEach(item => item.classList.remove('active'));
    }
});
wordpress.addEventListener('click', function wpActive(){
    let wp = document.querySelectorAll('.images > img:nth-child(4), .images > img:nth-child(8), .images > img:nth-child(12)');
    wp.forEach(item => item.classList.add('active'));
    if(!wp.forEach(item => item.classList.contains('active'))){
        document.querySelectorAll('.images > img:not(.images > img:nth-child(4), .images > img:nth-child(8), .images > img:nth-child(12))').forEach(item => item.classList.remove('active'));
    }
});
function hoverActive(){
    hover.forEach(item => item.classList.add('active'));
}
hoverActive();
allCtg.addEventListener('click', hoverActive);

// Viewers section

let images = document.querySelectorAll('.images-slider > img');
let review = document.querySelectorAll('.viewer-text');
let names = document.querySelectorAll('.viewer-name');
let jobs = document.querySelectorAll('.job');
let leftArrow = document.querySelector('#left');
let rigthArrow = document.querySelector('#right');
let imgOffset = 0;
let textOffset = 0;
let viewerOffset = 0;

rigthArrow.addEventListener('click', function(){
    imgOffset += 100;
    textOffset += 1160;
    viewerOffset += 120;
    images.forEach(function(item){
        item.style.right = imgOffset + 'px';
        item.style.left = imgOffset - 'px';
    });
    review.forEach(function(item){
        item.style.right = textOffset + 'px';
        item.style.left = textOffset - 'px';
    });
    names.forEach(function(item){
        item.style.right = viewerOffset + 'px';
        item.style.left = viewerOffset - 'px';
    });
    jobs.forEach(function(item){
        item.style.right = viewerOffset + 'px';
        item.style.left = viewerOffset - 'px';
    });
    if(imgOffset > 200){
        imgOffset = -100;
    }
    if(textOffset > 2320){
        textOffset = -1160;
    }
    if(viewerOffset > 240){
        viewerOffset = -120;
    }
});
leftArrow.addEventListener('click', function(){
    imgOffset -= 100;
    textOffset -= 1160;
    viewerOffset -= 120;
    images.forEach(function(item){
        item.style.right = imgOffset + 'px';
        item.style.left = -imgOffset - 'px';
    });
    review.forEach(function(item){
        item.style.right = textOffset + 'px';
        item.style.left = -textOffset - 'px';
    });
    names.forEach(function(item){
        item.style.right = viewerOffset + 'px';
        item.style.left = -viewerOffset - 'px';
    });
    jobs.forEach(function(item){
        item.style.right = viewerOffset + 'px';
        item.style.left = -viewerOffset - 'px';
    });
    if(imgOffset < -0){
        imgOffset = 400;
    }
    if(textOffset < -0){
        textOffset = 4660;
    }
    if(viewerOffset < -0){
        viewerOffset = 480;
    }
});